# CHANGELOG

## 0.12.5

- feat: custom checks can now be expressed with a new syntax, directly inside 
  the field definition 
  ([MR72](https://gitlab.com/validata-table/validata-table/-/merge_requests/72))
- feat: custom checks can now reference any format of the 
  [fr-format](https://github.com/datagouv/fr-format) library, and data will be 
  validated against this format. (`french-siret-value`, `french-siren-value` 
  and `nomenclature-actes-value` are soft-depreciated in favor of their 
  fr-format counterpart `siret`, `siren` and `nomenclature-acte`) 
  ([MR72](https://gitlab.com/validata-table/validata-table/-/merge_requests/72))

- doc: simplify documentation, separate documentation for the user and for the 
  developper

- fix: frictionless unexpected exceptions are handled and put into a 
  validation report ([Issue 
  162](https://gitlab.com/validata-table/validata-table/-/issues/162))
- fix: more accurate tags for custom checks 
  ([MR70](https://gitlab.com/validata-table/validata-table/-/merge_requests/70))

## 0.12.4

- fix(api): include the missing "title" property in the errors of the API 
  response

## 0.12.3

- feat: adds `include_resource_data` as an API parameter, to include the data 
  of the validated resource in the response 
  ([MR66](https://gitlab.com/validata-table/validata-table/-/merge_requests/66))

## 0.12.2

- fix: support of pydantic 2.10.3, and fix of the production image 
  ([MR64](https://gitlab.com/validata-table/validata-table/-/merge_requests/64))

## 0.12.1

- feat: metada (`version` and `date`, which is actually a timestamp) are now 
  also included for an error message. Timestamp is provided with 
  `Europe/Paris` timezone.

- fix: `compare-column-values` does not return a validation error when a 
  non-required value/column is missing. 
  ([Issue 142](https://gitlab.com/validata-table/validata-table/-/issues/142))
- fix: `acte-nomenclatures-value` errors do not trigger a ValueError
  [MR61](https://gitlab.com/validata-table/validata-table/-/merge_requests/61))


## 0.12.0

BREAKING CHANGES

- feat!: validation report simplification 
- feat!: `header_case` option is changed to `ignore_header_case` (in UI, HTTP 
  API and python API)

- feat: enhancement of the openapi specification and swagger documentation
- feat: improvements and better coverage for translations

- fix: validation does not fail with missing `title` schema property ([Issue 
  140](https://gitlab.com/validata-table/validata-table/-/issues/140))
- fix: correct error message for non-iso dates ([Issue 
  147](https://gitlab.com/validata-table/validata-table/-/issues/147))


## 0.11.4

- fix: properly install frictionless with excel support 
  ([MR50](https://gitlab.com/validata-table/validata-table/-/merge_requests/50))
- fix: properly run tests in CI

## 0.11.3

- fix: invalid schemas do not trigger infinite redirection loops 
  ([MR48](https://gitlab.com/validata-table/validata-table/-/merge_requests/48))
- fix: schemas with (unspecified) "path" property do not raise an error
  ([MR49](https://gitlab.com/validata-table/validata-table/-/merge_requests/49))

## 0.11.2

- feat: alpha release of support of the "frFormat" property to reference a 
  validation through the ["fr-format" 
  package](https://github.com/datagouv/fr-format) 
  ([MR37](https://gitlab.com/validata-table/validata-table/-/merge_requests/37))
- feat(ui): partial German translation of Validata UI ([MR 
  25](https://gitlab.com/validata-table/validata-table/-/merge_requests/25))

- clean-up of the broken pdf-extraction feature 
  ([MR41](https://gitlab.com/validata-table/validata-table/-/merge_requests/41))
- deployment procedure to Pypi restored 
  ([MR45](https://gitlab.com/validata-table/validata-table/-/merge_requests/45))

## 0.11.1

- feat(ci): deployment to Pypi temporarily suspended, as long as a fork of 
  frictionless is used

## 0.11.0

- feat: remote file formats with urls without file extensions are infered from Content-Type header [MR34](https://gitlab.com/validata-table/validata-table/-/merge_requests/34)
- fix: invalid dates do not throw a python error [MR31](https://gitlab.com/validata-table/validata-table/-/merge_requests/31)
- refacto: use frictionless v5 [MR27](https://gitlab.com/validata-table/validata-table/-/merge_requests/27) (currently : fork, waiting for [bug](https://github.com/frictionlessdata/frictionless-py/pull/1615) [fixes](https://github.com/frictionlessdata/frictionless-py/pull/1641) to be merged)
- refacto: tend towards hexagonal architecture

## 0.10.3

Edit gitlab CI configuration: include build docker images and push to gitlab container registry 
step to build the two Docker images validata-table-ui and validata-table-api (respectively
used for Validata UI and Validata API) and host them on gitlab container registry
automatically at new tag of release

## 0.10.2

- Improve dockerization process to remove source code in docker images created
- Add linting tools in CI such as `black`, `flake8`, `isort` and fix errors associated
- Improve code coverage in tests
- Fix static typing errors identified with `pyright` tool

## 0.10.1

Edit gitlab CI: use `twine` tool to publish `validata-table` package in PyPI

## 0.10.0

Init `validata-table` package with three subpackages `validata_core`,
`validata_ui` and `validata_api`, as a consequence of merging in the same
gitlab mono-repository [Validata Table](https://gitlab.com/validata-table/validata-table)
the three gitlab repositories:
- [Validata core](https://gitlab.com/validata-table/validata-core)
- [Validata API](https://gitlab.com/validata-table/validata-api)
- [Validata UI](https://gitlab.com/validata-table/validata-ui)

The init number of version 0.10.0 is resulting from this 
[discussion](https://gitlab.com/validata-table/validata-table/-/issues/28#note_1599202925)
