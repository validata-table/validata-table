# Errors

## frictionless errors by code

Validata transforms frictionless English error messages into detailed French 
messages, and aims at providing all the details so that the error is easily 
understood by a layman.

## Validata warnings and errors

In order to provide a better experience to tabular file providers, Validata 
use `schema_sync` frictionless option to ignore all errors relative to 
missing, misordered ou extranumerary columns.

These type of errors are reported by Validata as warnings instead:

- `missing-header-warn`: a column defined in the schema is not found in tabular data
- `extra-header-warn`: a column not defined in the schema is found in tabular data
- `disordered-header-warn`: column list doesn't follow schema order
  - this error is not emitted while there still missing or extra header warns

Of course a missing column with a `required` constraint will continue to be 
reported as an error.
