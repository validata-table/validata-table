from validata_api.api.json_errors import format_args


def test_format_args():
    args = {
        "schema": "https://schema.com",
        "url": "https://data.com",
        "ignore_header_case": "true",
        "include_resource_data": "false",
    }

    expected_formatted_args = {
        "schema": "https://schema.com",
        "url": "https://data.com",
        "options": {"ignore_header_case": "true", "include_resource_data": "false"},
    }

    assert format_args(args) == expected_formatted_args
