import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


def _schema_cohesive_columns(col1_type: str):
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "type": "number"},
            {"name": "col1", "type": col1_type},
            {"name": "col2", "type": "string"},
        ],
        "custom_checks": [
            {
                "name": "cohesive-columns-value",
                "params": {"column": "col1", "othercolumns": ["col2"]},
            }
        ],
    }


@pytest.fixture
def schema_cohesive_columns():
    return _schema_cohesive_columns("string")


@pytest.fixture
def schema_cohesive_columns_integer():
    return _schema_cohesive_columns("integer")


def test_cohesive_columns_values_valid(
    schema_cohesive_columns, schema_cohesive_columns_integer
):
    test_cases = [
        {
            "name": "both None",
            "source": [["id", "col1", "col2"], [1, None, None]],
        },
        {
            "name": "both values",
            "source": [["id", "col1", "col2"], [1, "foo", "bar"]],
        },
        {
            "name": "empty string is considered missing",
            "source": [["id", "col1", "col2"], [1, "", None]],
        },
        {
            "name": "both columns missing is valid",
            "source": [["id"], [1]],
        },
        {
            "name": "integer 0 is not considered missing",
            "col1_type": "integer",
            "source": [["id", "col1", "col2"], [1, 0, "bar"]],
        },
    ]
    for tc in test_cases:
        if "col1_type" in tc and tc["col1_type"] == "integer":
            schema = schema_cohesive_columns_integer
        else:
            schema = schema_cohesive_columns

        report = validate(tc["source"], schema)
        assert report.valid, report.to_dict()


def test_cohesive_columns_values_invalid(schema_cohesive_columns):
    sources = [
        [["id", "col1", "col2"], [1, "foo", None]],
        [["id", "col1", "col2"], [1, None, "bar"]],
        [["id", "col1", "col2"], [1, "", "bar"]],
    ]

    for source in sources:
        report = validate(source, schema_cohesive_columns)
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == ErrType.COHESIVE_COLUMNS_VALUE


@pytest.fixture
def schema_cohesive_columns_on_required_field():
    schema_cohesive_on_required_field = _schema_cohesive_columns("string")
    schema_cohesive_on_required_field["fields"][1]["constraints"] = {"required": True}
    return schema_cohesive_on_required_field


def test_cohesive_columns_values_required(schema_cohesive_columns_on_required_field):
    source = [["id", "col1", "col2"], [1, None, "bar"]]
    report = validate(source, schema_cohesive_columns_on_required_field)
    assert not report.valid
    errors = report.errors
    assert len(errors) == 2
    assert errors[0].type == ErrType.CONSTRAINT_ERROR
    assert errors[1].type == ErrType.COHESIVE_COLUMNS_VALUE


def test_cohesive_columns_values_missing_columns(
    schema_cohesive_columns_on_required_field,
):
    # GIVEN One of the column (not required) relative to the custom check (col2) does not exist
    # EXPECT the check to issue an error
    source = [["id", "col1"], [1, "foo"]]
    report = validate(source, schema_cohesive_columns_on_required_field)
    assert not report.valid
    utils.assert_single_error(report)

    error = report.errors[0]

    # Ensure that the error is a check-error related to the fact that one column included in the custom check is missing ("col1")

    assert error.type == ErrType.COHESIVE_COLUMNS_VALUE
    expected_message = "'col2'"
    assert expected_message in error.message


def test_cohesive_columns_values_required_missing_columns(
    schema_cohesive_columns_on_required_field,
):
    # GIVEN One ("col1") of the custom check columns specified as required does not exist
    # EXPECT validation error with two errors :
    # - one missing label error
    # - one check-error related to the fact that one column included in the custom check is missing ("col1")

    source = [["id", "col2"], [1, "bar"]]
    report = validate(source, schema_cohesive_columns_on_required_field)

    assert not report.valid
    errors = report.errors

    assert len(errors) == 2

    # Ensure that one error is a missing-label-error related to the missing required header column ("col1")
    assert errors[0].type == ErrType.MISSING_LABEL
    # # Ensure that one error is a check-error related to the fact that one column included in the custom check is missing ("col1")
    assert errors[1].type == ErrType.COHESIVE_COLUMNS_VALUE
    expected_message = "'col1'"
    assert expected_message in errors[1].message
