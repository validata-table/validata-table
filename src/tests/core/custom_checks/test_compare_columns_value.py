import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def schema_compare_columns():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "depenses", "title": "Dépenses", "type": "number"},
            {"name": "recettes", "title": "Recettes", "type": "number"},
        ],
        "custom_checks": [
            {
                "name": "compare-columns-value",
                "params": {"column": "depenses", "op": "<=", "column2": "recettes"},
            }
        ],
    }


def test_compare_columns_value_valid(schema_compare_columns):
    sources = [
        [["other"], [1]],
        [["depenses"], [12000]],
        [["depenses", "recettes"], [12000, 15000]],
        [["depenses", "recettes"], [12000, 12000]],
        # Unrequired column with empty cells: ignore custom check
        [["depenses", "recettes"], [12000, None]],
        [["depenses", "recettes"], [None, 6000]],
    ]
    for source in sources:
        report = validate(source, schema_compare_columns)
        assert report.valid


def test_compare_columns_value_invalid(schema_compare_columns):
    source = [["depenses", "recettes"], [12000, 6000]]
    report = validate(source, schema_compare_columns)
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.COMPARE_COLUMNS_VALUE


@pytest.fixture
def schema_compare_columns_for_none_value(schema_compare_columns):
    schema_compare_columns_for_none_value = schema_compare_columns
    schema_compare_columns_for_none_value["fields"].append(
        {
            "name": "A",
            "title": "Field A",
            "type": "string",
            "constraints": {"required": True},
        }
    )
    return schema_compare_columns_for_none_value


# To succeed this test, the resource tested needs to have at least one value in row containing None values,
# otherwise, an error 'blank-row' occurs from frictionless and appears in the validation report.
def test_ignore_custom_check_compare_columns_value_on_missing_values_on_optional_field(
    schema_compare_columns_for_none_value,
):
    source_both_none = [["A", "depenses", "recettes"], ["a", None, None]]
    report = validate(source_both_none, schema_compare_columns_for_none_value)
    assert report.valid


@pytest.fixture
def schema_compare_columns_on_required_field(schema_compare_columns):
    schema_compare_columns_on_required_field = schema_compare_columns
    schema_compare_columns_on_required_field["fields"][0]["constraints"] = {
        "required": True
    }
    return schema_compare_columns_on_required_field


def test_apply_compare_columns_value_on_missing_values_on_required_field(
    schema_compare_columns_on_required_field,
):
    # GIVEN an empty cell occuring on a required column ("depenses") related to the custom chech 'compare-columns-value'
    # EXPECT invalid report, validation ignoring custom check and reporting with one single
    # error 'constraint-error' related to the empty cell.
    source = [["depenses", "recettes"], [None, 6000]]
    report = validate(source, schema_compare_columns_on_required_field)
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.CONSTRAINT_ERROR
    assert error.title == "Valeur manquante"


def test_compare_columns_value_validate_start2(schema_compare_columns):
    # All columns relatives to the custom check does not exist -> custom check is ignored
    schema = schema_compare_columns
    source = [["A"], [7000]]
    report = validate(source, schema_descriptor=schema)
    assert report.valid


@pytest.fixture
def schema_with_wrong_operator(schema_compare_columns):
    schema_with_wrong_operator = schema_compare_columns
    schema_with_wrong_operator["custom_checks"][0]["params"][
        "op"
    ] = "opérateur_non_géré"
    return schema_with_wrong_operator


def test_wrong_operator_to_compare_columns_value(
    schema_with_wrong_operator,
):
    source = [["depenses", "recettes"], [12000, 13000]]
    report = validate(source, schema_with_wrong_operator)
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.CHECK_ERROR
    expected_message = "L'opérateur 'opérateur_non_géré' n'est pas géré."
    assert expected_message in error.message
