import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def schema_coordinates():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "coordonneesXY",
                "title": "Coordonnees GPS (longitude, latitude)",
                "type": "geopoint",
            },
        ],
        "custom_checks": [
            {"name": "french-gps-coordinates", "params": {"column": "coordonneesXY"}}
        ],
    }


@pytest.fixture
def schema_coordinates_array():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "coordonneesXY",
                "title": "Coordonnees GPS (longitude, latitude)",
                "type": "geopoint",
                "format": "array",
            },
        ],
        "custom_checks": [
            {"name": "french-gps-coordinates", "params": {"column": "coordonneesXY"}}
        ],
    }


def test_french_coordinates_valid(schema_coordinates, schema_coordinates_array):
    test_cases = [
        {
            # French coordinates
            "source": [["coordonneesXY"], ["2.294481, 48.85837"]],
            "schema": schema_coordinates,
        },
        {
            # French coordinates as array
            "source": [["coordonneesXY"], ["[2.294481, 48.85837]"]],
            "schema": schema_coordinates_array,
        },
    ]

    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        assert report.valid


def test_french_coordinates_invalid(schema_coordinates):
    test_cases = [
        {
            # Reversed coordinates
            "source": [["coordonneesXY"], ["48.85837, 2.294481"]],
            "expected_type": ErrType.REVERSED_FRENCH_GPS_COORDINATES,
        },
        {
            # Non french coordinates
            "source": [["coordonneesXY"], ["48.85837, 52.0"]],
            "expected_type": ErrType.FRENCH_GPS_COORDINATES,
        },
    ]
    for tc in test_cases:
        report = validate(tc["source"], schema_coordinates)
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == tc["expected_type"]


@pytest.fixture
def schema_coordinates_for_none_value():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "coordonneesXY",
                "title": "Coordonnees GPS (longitude, latitude)",
                "type": "geopoint",
            },
        ],
        "custom_checks": [
            {"name": "french-gps-coordinates", "params": {"column": "coordonneesXY"}}
        ],
    }


# To succeed this test, the resource tested needs to have at least one value in row containing None values,
# otherwise, an error 'blank-row' occurs from frictionless and appears in the validation report.
def test_do_not_apply_french_gps_coordindates_on_missing_values_on_optional_field(
    schema_coordinates_for_none_value,
):
    source = [["A", "coordonneesXY"], ["a", "2.294481, 48.85837"], ["a", None]]
    report = validate(source, schema_coordinates_for_none_value)
    assert report.valid


@pytest.fixture
def schema_coordinates_on_required_field():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "coordonneesXY",
                "title": "Coordonnees GPS (longitude, latitude)",
                "type": "geopoint",
                "constraints": {"required": True},
            },
        ],
        "custom_checks": [
            {"name": "french-gps-coordinates", "params": {"column": "coordonneesXY"}}
        ],
    }


# To succeed this test, the resource tested needs to have at least one value in row containing None values,
# otherwise, an error 'blank-row' occurs from frictionless and appears in the validation report.
def test_french_gps_coordindates_on_missing_values_when_required_field(
    schema_coordinates_on_required_field,
):
    source = [["A", "coordonneesXY"], ["a", "2.294481, 48.85837"], ["a", None]]
    report = validate(source, schema_coordinates_on_required_field)
    assert not report.valid
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.CONSTRAINT_ERROR
    assert error.title == "Valeur manquante"
