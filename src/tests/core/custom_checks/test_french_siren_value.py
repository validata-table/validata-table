import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def schema_required_field_and_siren_custom_check():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "siren",
                "title": "N° SIREN",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "B",
                "title": "Field B",
                "type": "string",
                "constraints": {"required": True},
            },
            {"name": "C", "title": "Field C", "type": "string"},
        ],
        "custom_checks": [
            {"name": "french-siren-value", "params": {"column": "siren"}}
        ],
    }


@pytest.fixture
def schema_siren():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "title": "id", "type": "number"},
            {"name": "siren", "title": "Numéro SIREN", "type": "string"},
        ],
        "custom_checks": [
            {"name": "french-siren-value", "params": {"column": "siren"}}
        ],
    }


def test_custom_check_siren_valid(schema_siren):
    sources = [
        [["id", "siren"], [1, "529173189"]],
        # Ignore custom check on empty column not required
        [["id", "siren"], [1, None]],
        # Ignore custom check on inexistent column relative to the custom check
        [["id"], [1]],
    ]
    for source in sources:
        report = validate(source, schema_siren)
        assert report.valid
        assert (
            len(report.warnings) >= 1 and "déprécié" in report.warnings[-1]
        ), "french-siren-value doit retourner un warning de dépréciation"


@pytest.fixture
def schema_siren_on_required_field():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "title": "id", "type": "number"},
            {
                "name": "siren",
                "title": "Numéro SIREN",
                "type": "string",
                "constraints": {"required": True},
            },
        ],
        "custom_checks": [
            {"name": "french-siren-value", "params": {"column": "siren"}}
        ],
    }


def test_custom_check_siren_invalid(
    schema_required_field_and_siren_custom_check,
    schema_siren,
    schema_siren_on_required_field,
):
    test_cases = [
        {
            # Missing required header 'B' and siren custom check constraint on field 'siren'
            "source": [["siren", "C"], ["226500015", "c"]],
            "schema": schema_required_field_and_siren_custom_check,
            "type_error_expected": ErrType.MISSING_LABEL,
            "title_error_expected": "Colonne obligatoire manquante",
        },
        {
            # Invalid siren value in data
            "source": [["id", "siren"], [1, "529173188"]],
            "schema": schema_siren,
            "type_error_expected": ErrType.CUSTOM_CHECK_ERROR,
        },
        {
            # Empty cell on required missing column related to custom check:
            # ignore custom check, only constraint-error related to empty cell is reported
            "source": [["id", "siren"], [1, None]],
            "schema": schema_siren_on_required_field,
            "type_error_expected": ErrType.CONSTRAINT_ERROR,
        },
        {
            # Missing required column related to custom check:
            # ignore custom check, only missing-label error reported
            "source": [["id"], [1]],
            "schema": schema_siren_on_required_field,
            "type_error_expected": ErrType.MISSING_LABEL,
        },
    ]

    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == tc["type_error_expected"]
