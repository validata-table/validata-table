import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def schema_siret():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "title": "id", "type": "number"},
            {"name": "numero_siret", "title": "Numéro SIRET", "type": "string"},
        ],
        "custom_checks": [
            {"name": "french-siret-value", "params": {"column": "numero_siret"}}
        ],
    }


def test_custom_check_siret_valid(schema_siret):
    sources = [
        [["id", "numero_siret"], [1, "83014132100026"]],
        # Ignore custom check on empty column not required
        [["id", "numero_siret"], [1, None]],
        # Ignore custom check on inexistent column relative to the custom check
        [["id"], [1]],
    ]

    for source in sources:
        report = validate(source, schema_siret)
        assert report.valid, report.to_dict()
        assert (
            len(report.warnings) >= 1 and "déprécié" in report.warnings[-1]
        ), "french-siret-value doit retourner un warning de dépréciation"


@pytest.fixture
def schema_siret_on_required_field():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "title": "id", "type": "number"},
            {
                "name": "numero_siret",
                "title": "Numéro SIRET",
                "type": "string",
                "constraints": {"required": True},
            },
        ],
        "custom_checks": [
            {"name": "french-siret-value", "params": {"column": "numero_siret"}}
        ],
    }


def test_custom_check_siret_invalid(schema_siret, schema_siret_on_required_field):
    test_cases = [
        {
            # Invalid siret value in data
            "source": [["id", "numero_siret"], [1, "529173188"]],
            "schema": schema_siret,
            "type_error_expected": ErrType.CUSTOM_CHECK_ERROR,
        },
        {
            # Empty cell on required missing column related to custom check:
            # ignore custom check, only constraint-error related to empty cell is reported
            "source": [["id", "numero_siret"], [1, None]],
            "schema": schema_siret_on_required_field,
            "type_error_expected": ErrType.CONSTRAINT_ERROR,
        },
    ]

    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == tc["type_error_expected"]
