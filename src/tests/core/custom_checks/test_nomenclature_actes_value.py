import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def schema_nomenclature_actes_value():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [{"name": "acte", "title": "Acte", "type": "string"}],
        "custom_checks": [
            {"name": "nomenclature-actes-value", "params": {"column": "acte"}}
        ],
    }


@pytest.fixture
def schema_nomenclature_actes_for_none_value():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {"name": "acte", "title": "Acte", "type": "string"},
        ],
        "custom_checks": [
            {"name": "nomenclature-actes-value", "params": {"column": "acte"}}
        ],
    }


@pytest.fixture
def schema_nomenclature_actes_value_on_required_field():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "acte",
                "title": "Acte",
                "type": "string",
                "constraints": {"required": True},
            },
        ],
        "custom_checks": [
            {"name": "nomenclature-actes-value", "params": {"column": "acte"}}
        ],
    }


def test_nomenclature_actes_values_valid(
    schema_nomenclature_actes_value, schema_nomenclature_actes_for_none_value
):
    test_cases = [
        {
            "source": [["acte"], ["Fonction publique/foobar"]],
            "schema": schema_nomenclature_actes_value,
        },
        {
            # Multi-case in value
            "source": [["acte"], ["fOnCtIOn pubLIQUE/foobar"]],
            "schema": schema_nomenclature_actes_value,
        },
        {
            # Ignore custom check on not required empty column
            "source": [["A", "acte"], ["a", None]],
            "schema": schema_nomenclature_actes_for_none_value,
        },
    ]
    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        assert report.valid
        assert (
            len(report.warnings) >= 1 and "déprécié" in report.warnings[-1]
        ), "nomenclature-actes-value doit retourner un warning de dépréciation"


def test_nomenclature_actes_values_invalid(
    schema_nomenclature_actes_value,
    schema_nomenclature_actes_value_on_required_field,
):
    test_cases = [
        {
            # Empty cell on required missing column related to custom check:
            # ignore custom check, only constraint-error related to empty cell is reported
            "source": [["A", "acte"], ["a", None]],
            "schema": schema_nomenclature_actes_value_on_required_field,
            "type_error_expected": ErrType.CONSTRAINT_ERROR,
            "title_expected": "Valeur manquante",
            "message_part_error_expected": "obligatoire",
        },
        {
            "source": [["acte"], ["random stuff"]],
            "schema": schema_nomenclature_actes_value,
            "type_error_expected": ErrType.CUSTOM_CHECK_ERROR,
            "title_expected": "Format Invalide",
            # Message is specialized on exact error
            "message_part_error_expected": "signe oblique « / » est manquant",
        },
        {
            "source": [["acte"], ["random stuff /"]],
            "schema": schema_nomenclature_actes_value,
            "type_error_expected": ErrType.CUSTOM_CHECK_ERROR,
            "title_expected": "Format Invalide",
            "message_part_error_expected": "pas reconnu",
        },
        {
            "source": [["acte"], ["Urbanisme /"]],
            "schema": schema_nomenclature_actes_value,
            "type_error_expected": ErrType.CUSTOM_CHECK_ERROR,
            "title_expected": "Format Invalide",
            "message_part_error_expected": "ne doit pas être précédé ni suivi d'espace",
        },
        {
            "source": [["acte"], ["Urbanisme /"]],
            "schema": schema_nomenclature_actes_value,
            "type_error_expected": ErrType.CUSTOM_CHECK_ERROR,
            "title_expected": "Format Invalide",
            "message_part_error_expected": "ne doit pas être précédé ni suivi d'espace",
        },
    ]
    for tc in test_cases:
        report = validate(tc["source"], tc["schema"])
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == tc["type_error_expected"]
        assert error.title == tc["title_expected"]
        assert tc["message_part_error_expected"] in error.message
