import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType, Tag
from validata_core.domain.types.table_region import involves_single_row


@pytest.fixture
def schema_one_of_required():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "id", "title": "Identifiant", "type": "number"},
            {"name": "A", "title": "Colonne A", "type": "string"},
            {"name": "B", "title": "Colonne B", "type": "string"},
        ],
        "custom_checks": [
            {
                "name": "one-of-required",
                "params": {"column1": "A", "column2": "B"},
            }
        ],
    }


def test_one_of_required_valid(schema_one_of_required):
    # Test cases expecting valid reports
    sources = [
        [["id", "A"], [1, "a"]],
        [["id", "B"], [1, "b"]],
        [["id", "A", "B"], [1, "a", "b"]],
        [["id", "A", "B"], [1, "a", None]],
        [["id", "A", "B"], [1, None, "b"]],
    ]

    for source in sources:
        report = validate(source, schema_one_of_required)
        assert report.valid


def test_one_of_required_invalid_row(schema_one_of_required):
    # Test cases expecting invalid reports with error of one-of-required custom check and message :
    # "Au moins l'une des colonnes 'A' ou 'B' doit comporter une valeur"
    # (Indirectly tests validate_row method)

    sources = [
        [["id", "A"], [1, None]],
        [["id", "B"], [1, None]],
        [["id", "A", "B"], [1, None, None]],
    ]
    for source in sources:
        report = validate(source, schema_one_of_required)
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == ErrType.ONE_OF_REQUIRED
        assert (
            "Au moins l'une des colonnes 'A' ou 'B' doit comporter une valeur"
            in error.message
        )


def test_one_of_required_missing_columns(schema_one_of_required):
    # Test cases expecting invalid reports with error of one-of-required custom check and message :
    # (Indirectly tests validate_start method)
    source = [["id"], [1]]
    report = validate(source, schema_one_of_required)
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.ONE_OF_REQUIRED
    assert "Une des deux colonnes requise : 'A' et 'B' sont manquantes" in error.message
    assert involves_single_row(error.location) and error.location.row_number == 1
    assert Tag.HEADER in error.tags
