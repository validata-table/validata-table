import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def schema_phone_number():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "telephone",
                "title": "Numéro de téléphone",
                "type": "string",
            },
        ],
        "custom_checks": [
            {"name": "phone-number-value", "params": {"column": "telephone"}}
        ],
    }


# https://en.wikipedia.org/wiki/Fictitious_telephone_number#France


def test_valid_10digits_french_phone_number(schema_phone_number):
    source = [["telephone"], ["02 61 91 13 45"]]
    report = validate(source, schema_phone_number)
    assert report.valid


def test_valid_10digits_french_phone_number_without_spaces(schema_phone_number):
    source = [["telephone"], ["0261911345"]]
    report = validate(source, schema_phone_number)
    assert report.valid


def test_valid_10digits_french_phone_number_with_extra_spaces(schema_phone_number):
    source = [["telephone"], ["  026  191  13  45  "]]
    report = validate(source, schema_phone_number)
    assert report.valid


def test_valid_international_phone_number(schema_phone_number):
    source = [["telephone"], ["+33 2 61 91 13 45"]]
    report = validate(source, schema_phone_number)
    assert report.valid


def test_valid_french_short_phone_number(schema_phone_number):
    source = [["telephone"], ["115"]]
    report = validate(source, schema_phone_number)
    assert report.valid


def test_invalid_legacy_french_phone_number(schema_phone_number):
    source = [["telephone"], ["619 13 45"]]
    report = validate(source, schema_phone_number)
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.PHONE_NUMBER_VALUE


@pytest.fixture
def schema_phone_number_for_none_value():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "telephone",
                "title": "Numéro de téléphone",
                "type": "string",
            },
        ],
        "custom_checks": [
            {"name": "phone-number-value", "params": {"column": "telephone"}}
        ],
    }


# To succeed this test, the resource tested needs to have at least one value in row containing None values,
# otherwise, an error 'blank-row' occurs from frictionless and appears in the validation report.
def test_do_not_apply_french_phone_number_on_missing_values_on_optional_field(
    schema_phone_number_for_none_value,
):
    source = [["A", "telephone"], ["a", None]]
    report = validate(source, schema_phone_number_for_none_value)
    assert report.valid


@pytest.fixture
def schema_phone_number_on_required_field():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {
                "name": "telephone",
                "title": "Numéro de téléphone",
                "type": "string",
                "constraints": {"required": True},
            },
        ],
        "custom_checks": [
            {"name": "phone-number-value", "params": {"column": "telephone"}}
        ],
    }


def test_apply_french_phone_number_on_missing_values_on_required_field(
    schema_phone_number_on_required_field,
):
    source = [["A", "telephone"], ["a", None]]
    report = validate(source, schema_phone_number_on_required_field)
    utils.assert_single_error(report)
    error = report.errors[0]
    assert error.type == ErrType.CONSTRAINT_ERROR
    assert error.title == "Valeur manquante"
