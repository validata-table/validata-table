import pytest

from tests.core import utils
from validata_core import validate
from validata_core.domain.types import ErrType


def _schema_sum_columns_value(column_list=["chauffage", "salaires", "fraisdebouche"]):
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "charges", "type": "number"},
            {"name": "chauffage", "type": "number"},
            {"name": "salaires", "type": "number"},
            {"name": "fraisdebouche", "type": "number"},
        ],
        "custom_checks": [
            {
                "name": "sum-columns-value",
                "params": {"column": "charges", "columns": column_list},
            }
        ],
    }


@pytest.fixture
def schema_sum_columns_value_ok():
    return _schema_sum_columns_value()


def test_valid_custom_sum_columns_value_1(schema_sum_columns_value_ok):
    sources = [
        [
            ["charges", "chauffage", "salaires", "fraisdebouche"],
            [12000, 600, 4000, 7400],
        ],
        [
            ["charges", "chauffage", "salaires", "fraisdebouche"],
            [12000, 600, None, 7400],
        ],
        [
            ["charges", "chauffage", "salaires", "fraisdebouche"],
            [None, 600, 4000, 7400],
        ],
    ]

    for source in sources:
        report = validate(source, schema_sum_columns_value_ok)
        assert report.valid


def test_invalid_custom_sum_columns_value_4(schema_sum_columns_value_ok):
    sources = [
        [
            ["charges", "chauffage", "salaires", "fraisdebouche"],
            # 1100 != 600 + 4000 + 7400
            [1100, 600, 4000, 7400],
        ],
        [
            ["charges", "chauffage", "salaires", "fraisdebouche"],
            # 1100 != 600 + 4000 + 0
            [1100, 600, 4000, 0],
        ],
    ]
    for source in sources:
        report = validate(source, schema_sum_columns_value_ok)
        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == ErrType.SUM_COLUMNS_VALUE
