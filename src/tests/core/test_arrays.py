from pathlib import Path

import pytest

from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture
def array_schema():
    return {
        "$schema": "https://specs.frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "array_field",
                "title": "Array field",
                "description": "Array field description",
                "type": "array",
                "arrayItem": {"constraints": {"enum": ["a", "b"]}},
            }
        ],
    }


def test_validate_ko(array_schema):
    report = validate(Path("tests/core/fixtures/invalid_array.csv"), array_schema)
    errors = report.errors
    assert len(errors) == 2
    assert errors[0].type == ErrType.CONSTRAINT_ERROR
    assert errors[1].type == ErrType.CONSTRAINT_ERROR
    assert not report.valid


def test_validate_ok(array_schema):
    report = validate(Path("tests/core/fixtures/valid_array.csv"), array_schema)
    assert report.valid
