import pytest

from tests.core import utils
from validata_core import resource_service, validate
from validata_core.domain.types import ErrType, TypedException


@pytest.fixture
def schema_abc():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "string",
                "constraints": {"required": True},
            },
            {"name": "B", "title": "Field B", "type": "string"},
            {"name": "C", "title": "Field C", "type": "string"},
        ],
    }


@pytest.fixture
def schema_types_and_required():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "A",
                "title": "Field A",
                "type": "number",
                "constraints": {"required": True},
            },
            {
                "name": "B",
                "title": "Field B",
                "type": "date",
                "constraints": {"required": True},
            },
        ],
    }


@pytest.fixture
def schema_number():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [{"name": "A", "title": "Field A", "type": "number"}],
    }


def validate_csv_bytes(csv_bytes_source, schema):
    validata_source = resource_service.from_file_content("foo.csv", csv_bytes_source)
    return validate(validata_source.to_inline_data(), schema)


def test_empty_file(schema_abc):
    source = b""
    with pytest.raises(TypedException) as err:
        validate_csv_bytes(source, schema_abc)
        assert err.type == ErrType.SOURCE_ERROR


def test_valid_delimiters(schema_abc):
    sources = [b"""A,B,Ca,b,c""", b"""A;B;Ca;b;c"""]

    for source in sources:
        report = validate_csv_bytes(source, schema_abc)
        utils.assert_valid_report(report)


def test_invalid_report(schema_abc):
    test_cases = [
        {
            # Empty required value
            "source": [["A", "B", "C"], ["", "b", "c"]],
            "error_type": ErrType.CONSTRAINT_ERROR,
        },
        {
            # Missing required column
            "source": [["B", "C"], ["b", "c"]],
            "error_type": ErrType.MISSING_LABEL,
        },
    ]
    for test_case in test_cases:
        report = validate(test_case["source"], schema_abc)
        assert not report.valid

        utils.assert_single_error(report)
        error = report.errors[0]
        assert error.type == test_case["error_type"]

        utils.assert_no_warning(report)


def test_valid_report_with_warnings(schema_abc):
    test_cases = [
        {
            # Column 'b' does not respect case on field "B"
            "source": [["A", "b", "C"], ["a", "b", "c"]],
            "expected_nb_warnings": 2,
            "expected_warning_messages": [
                "Colonne manquante : Ajoutez la colonne manquante `B`.",
                "Colonne surnuméraire : Retirez la colonne `b` non définie dans le schéma.",
            ],
        },
        {
            # Missing column "B" which is not required in the schema fields
            "source": [["A", "C"], ["a", "c"]],
            "expected_nb_warnings": 1,
            "expected_warning_messages": [
                "Colonne manquante : Ajoutez la colonne manquante `B`."
            ],
        },
        {
            # Extra column "D"
            "source": [["A", "D", "B", "C"], ["a", "d", "b", "c"]],
            "expected_nb_warnings": 1,
            "expected_warning_messages": [
                "Colonne surnuméraire : Retirez la colonne `D` non définie dans le schéma."
            ],
        },
        {
            # Disordered columns
            "source": [["C", "A", "B"], ["c", "a", "b"]],
            "expected_nb_warnings": 1,
            "expected_warning_messages": [
                "Colonnes désordonnées : Réordonnez les colonnes du fichier pour respecter le schéma : ['A', 'B', 'C']."
            ],
        },
    ]
    for test_case in test_cases:
        report = validate(test_case["source"], schema_abc)
        utils.assert_valid_report(report)

        warnings = report.warnings
        assert len(warnings) == test_case["expected_nb_warnings"]
        for warn, expected_message in zip(
            warnings, test_case["expected_warning_messages"]
        ):
            assert warn == expected_message


def test_ignore_case():
    schema = {
        "fields": [
            {"name": "AA", "constraints": {"required": True}},
            {"name": "BB", "constraints": {"required": True}},
            {"name": "CC", "constraints": {"required": True}},
        ]
    }

    source = [
        ["AA", "bb", "Cc"],
        ["a", "b", "c"],
    ]

    # Test ignore case
    report = validate(source, schema, ignore_header_case=True)

    utils.assert_valid_report(report)
    utils.assert_no_warning(report)

    # Test sensitive to the case
    report = validate(source, schema)
    assert not report.valid

    errors = report.errors
    assert len(errors) == 2
    assert report.stats.errors == 2

    expected_errors_messages = [
        "La colonne obligatoire `BB` est manquante",
        "La colonne obligatoire `CC` est manquante",
    ]
    for error, expected_message in zip(errors, expected_errors_messages):
        assert error.type == ErrType.MISSING_LABEL
        assert error.message == expected_message

    warnings = report.warnings
    assert len(warnings) == 2
    assert report.stats.warnings == 2

    expected_warnings_messages = [
        "Colonne surnuméraire : Retirez la colonne `bb` non définie dans le schéma.",
        "Colonne surnuméraire : Retirez la colonne `Cc` non définie dans le schéma.",
    ]

    for warn, expected_message in zip(warnings, expected_warnings_messages):
        assert warn == expected_message


def test_ignore_case_missing_columns_not_required():
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "A", "constraints": {"required": True}},
            {"name": "B"},
            {"name": "C"},
        ],
    }

    source = [["b", "c"], ["foo", "bar"]]

    report = validate(source, schema, ignore_header_case=True)
    assert not report.valid

    utils.assert_single_error(report)
    assert report.errors[0].type == ErrType.MISSING_LABEL

    utils.assert_no_warning(report)


def test_validate_with_schema_from_descriptor():
    schema = "tests/core/fixtures/schema_one_single_field.json"
    test_cases = [
        {"source": [["A"], ["a"]], "expected": True},
        {
            "source": [["B"], ["b"]],
            "expected": False,
        },  # missing "A" column required
    ]

    assert isinstance(schema, str) and not schema.startswith("http")
    for tc in test_cases:
        report = validate(tc["source"], schema)
        assert report.valid == tc["expected"]
