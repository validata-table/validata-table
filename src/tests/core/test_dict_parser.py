from typing import List

from validata_core.domain.dict_parser import DictParser


def test_dict_parser():
    test_cases = [
        {
            "name": "Key Error with non-existing key and no default 1",
            "from_data": {},
            "get": "field",
            "as_type": int,
            "expect_error": KeyError,
        },
        {
            "name": "Key Error with non-existing key and no default 2",
            "from_data": {"field": 3},
            "get": "other_field",
            "as_type": int,
            "expect_error": KeyError,
        },
        {
            "name": "Default value if non-existing key with default",
            "from_data": {"field": 3},
            "get": "other_field",
            "default": 3,
            "as_type": int,
            "expect_value": 3,
        },
        {
            "name": "Actual value for right field and int type (no default)",
            "from_data": {"field": 3},
            "get": "field",
            "as_type": int,
            "expect_value": 3,
        },
        {
            "name": "Actual value for right field and int type (with default)",
            "from_data": {"field": 3},
            "get": "field",
            "default": 42,
            "as_type": int,
            "expect_value": 3,
        },
        {
            "name": "Actual value for right field and str type (no default)",
            "from_data": {"some_other_field": 3, "field": "abc"},
            "get": "field",
            "as_type": str,
            "expect_value": "abc",
        },
        {
            "name": "TypeError if type mismatch 1  (no default)",
            "from_data": {"field": 3},
            "get": "field",
            "as_type": bool,
            "expect_error": TypeError,
        },
        {
            "name": "TypeError if type mismatch 2  (no default)",
            "from_data": {"field": True},
            "get": "field",
            "as_type": float,
            "expect_error": TypeError,
        },
        {
            "name": "Actual value for right field and List[str] type",
            "from_data": {"columns": ["a", "b", "c"]},
            "get": "columns",
            "as_type": List[str],
            "expect_value": ["a", "b", "c"],
        },
        {
            "name": "Actual value for right field and List[int] type",
            "from_data": {"columns": [1, 2, 3]},
            "get": "columns",
            "as_type": List[int],
            "expect_value": [1, 2, 3],
        },
        {
            "name": "TypeError for list of wrong type",
            "from_data": {"columns": [1, 2, 3]},
            "get": "columns",
            "as_type": List[str],
            "expect_error": TypeError,
        },
    ]

    for tc in test_cases:
        dict_parser = DictParser(tc["from_data"])

        try:
            if "default" in tc:
                value = dict_parser.get(tc["get"], tc["as_type"], tc["default"])
            else:
                value = dict_parser.get(tc["get"], tc["as_type"])

        except Exception as e:
            if "expect_error" not in tc or type(e) is not tc["expect_error"]:
                assert (
                    False
                ), f'Unexpected {type(e).__name__} for test case {tc["name"]}'
            continue

        assert (
            "expect_value" in tc and value == tc["expect_value"]
        ), f'Unexpected value for test case {tc["name"]}'
