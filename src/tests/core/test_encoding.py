import pytest

from validata_core import resource_service
from validata_core.domain.types import ErrType, TypedException


@pytest.fixture
def irve_accent_valide_content():
    return open("tests/core/fixtures/irve_accent_valide.csv", "rb").read()


@pytest.fixture
def irve_driveco_content():
    return open("tests/core/fixtures/irve-driveco-201215.csv", "rb").read()


@pytest.fixture
def with_bom_content():
    return open("tests/core/fixtures/sample_with_bom.csv", "rb").read()


@pytest.fixture
def invalid_utf16_content():
    return open(
        "tests/core/fixtures/mini-menus-collectifs_invalide.utf-16.csv",
        "rb",
    ).read()


def test_detect_utf_8(irve_accent_valide_content):
    table = resource_service.from_file_content("foo.csv", irve_accent_valide_content)
    header = table.header()
    assert "accessibilité" in header, f"Probléme d'encodage : {header}"


def test_detect_iso8859_1(irve_driveco_content):
    table = resource_service.from_file_content("foo.csv", irve_driveco_content)
    header = table.header()
    assert "accessibilité" in header, f"Probléme d'encodage : {header}"


def test_ignore_bom(with_bom_content):
    table = resource_service.from_file_content("foo.csv", with_bom_content)
    header = table.header()
    assert header
    assert header[0] == "id", f"Probléme de gestion de BOM : {header}"


def test_invalid_utf16(invalid_utf16_content):
    with pytest.raises(TypedException) as err:
        resource_service.from_file_content(
            "invalid_utf16.csv", invalid_utf16_content
        ).header()
        assert err.type == ErrType.SOURCE_ERROR

    with pytest.raises(TypedException) as err:
        resource_service.from_file_content(
            "invalid_utf16.csv", invalid_utf16_content
        ).rows()
        assert err.type == ErrType.SOURCE_ERROR
