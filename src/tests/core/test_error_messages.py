from typing import Any, Tuple

import pytest

from validata_core import validate

from .utils import SchemaBuilder


def test_incorrect_date_message():
    schema_date_format = SchemaBuilder().add_field("A", "date", format="%Y-%m-%d").get()

    date = "37/03/2024"
    source = [["A"], [date]]

    try:
        validate(source, schema_date_format)
    except Exception as e:
        pytest.fail(f"Validation fails with date { date }: { e } ")


def test_invalid_types_with_no_explicit_format():
    test_cases = [
        {
            "type": "date",
            "source": [["A"], ["not a date"]],
            "expected_error": "Format de date incorrect",
        },
        {
            "type": "string",
            "source": [["A"], [3]],
            "expected_error": "Format de texte incorrect",
        },
    ]

    for tc in test_cases:
        schema = SchemaBuilder().add_field("A", tc["type"]).get()
        report = validate(tc["source"], schema)
        print(report.to_dict())
        assert len(report.errors) == 1

        error = report.errors[0]
        assert error.title == tc["expected_error"]


def test_enum_message_includes_valid_values():
    ENUM1, ENUM2 = ("EnumValue1", "EnumValue2")
    schema_with_enum = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "Enum",
                "type": "string",
                "constraints": {"enum": [ENUM1, ENUM2]},
            }
        ],
    }

    wrong_value = "notAnEnumValue"
    source = [["Enum"], [wrong_value]]

    report = validate(source, schema_with_enum)

    assert len(report.errors) == 1
    error = report.errors[0]

    assert ENUM1 in error.message
    assert ENUM2 in error.message


def test_error_type_message():
    class FakeLocale:
        def __init__(self, title: str):
            self.title = title

        def enum(self, _: str) -> Tuple[str, str]:
            return self.title, ""

    ENUM1, ENUM2 = ("EnumValue1", "EnumValue2")
    schema_with_enum = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "Enum",
                "type": "string",
                "constraints": {"enum": [ENUM1, ENUM2]},
            }
        ],
    }

    wrong_value = "notAnEnumValue"
    source = [["Enum"], [wrong_value]]

    forced_title_in_locale = ["Format incorrect", "Contrainte non respectée"]
    for expected_title in forced_title_in_locale:
        fakeLocale: Any = FakeLocale(expected_title)

        report = validate(source, schema_with_enum, locale=fakeLocale)

        assert len(report.errors) == 1
        error = report.errors[0]

        assert error.title == expected_title


def test_locale_properly_gets_type():
    class FakeLocale:
        """A locale that records what functions are called with what
        arguments, for testing purposes"""

        def __init__(self):
            self.method_called = None
            self.true_values = None
            self.false_values = None
            self.enum_values = None
            self.format = None

        def boolean_type(self, true_values, false_values):
            self.method_called = "boolean_type"
            self.true_values = true_values
            self.false_values = false_values
            return "", ""

        def array_type(self):
            self.method_called = "array_type"
            return "", ""

        def list_type(self, *args, **kwargs):
            self.method_called = "list_type"
            return "", ""

        def string_type(self, format):
            self.method_called = "string_type"
            self.format = format
            return "", ""

        def enum(self, enum_values):
            self.method_called = "enum"
            self.enum_values = enum_values
            return "", ""

    test_cases = [
        {
            "name": "boolean type is properly handled by locale",
            "schema_type": "boolean",
            "expected_locale_method": "boolean_type",
            "additional_options": {},
            "wrong_value": 3,
            "expected_true_values": [
                "true",
                "True",
                "TRUE",
                "1",
            ],  # table schema default
            "expected_false_values": [
                "false",
                "False",
                "FALSE",
                "0",
            ],  # table schema default
        },
        {
            "name": "boolean type is properly handled by locale, with trueValues and falseValues",
            "schema_type": "boolean",
            "expected_locale_method": "boolean_type",
            "additional_options": {"trueValues": ["1"], "falseValues": ["0"]},
            "wrong_value": 3,
            "expected_true_values": ["1"],
            "expected_false_values": ["0"],
        },
        {
            "name": "array type is properly handled by locale",
            "schema_type": "array",
            "expected_locale_method": "array_type",
            "additional_options": {},
            "wrong_value": 3,
        },
        {
            "name": "enum constraint is properly handled",
            "schema_type": "string",
            "expected_locale_method": "enum",
            "additional_options": {"constraints": {"enum": ["A", "B"]}},
            "wrong_value": "C",
            "expected_enum_values": ["A", "B"],
        },
        {
            "name": "string constraint is properly handled",
            "schema_type": "string",
            "expected_locale_method": "string_type",
            "additional_options": {"format": "email"},
            "wrong_value": "a",
            "expected_format": "email",
        },
        # COMMENTED as list type is not yet supported in frictionless
        # (introduced in frictionless v2)
        # {
        #     "name": "list type is properly handled by locale",
        #     "schema_type": "list",
        #     "additional_options": {"itemType": "integer"},
        #     "wrong_value": "A,B",
        # },
    ]
    for tc in test_cases:
        schema = {
            "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
            "fields": [
                {"name": "field", "type": tc["schema_type"], **tc["additional_options"]}
            ],
        }

        fakeLocale: Any = FakeLocale()

        report = validate([["field"], [tc["wrong_value"]]], schema, locale=fakeLocale)
        # Call message to trigger translation
        [err.message for err in report.errors]

        assert (
            fakeLocale.method_called == tc["expected_locale_method"]
        ), f'Test fails : {tc["name"]}'
        if "expected_true_values" in tc:
            assert fakeLocale.true_values == tc["expected_true_values"]
        if "expected_false_values" in tc:
            assert fakeLocale.false_values == tc["expected_false_values"]
        if "expected_enum_values" in tc:
            assert fakeLocale.enum_values == tc["expected_enum_values"]
        if "expected_format" in tc:
            assert fakeLocale.format == tc["expected_format"]
