import json
from datetime import datetime

import pytest

from validata_core import Report


def test_format_report():
    test_cases = [
        {  # test with false stats errors
            "name": "No error, no warning",
            "report": Report(
                errors=[],
                warnings=[],
                stats_seconds=0,
                stats_fields=0,
                stats_rows=0,
                stats_rows_processed=0,
            ),
        }
    ]

    for tc in test_cases:
        report = tc["report"]
        formatted_report = report.format()
        report_dict = formatted_report["report"]

        m = f"Test case '{ tc['name'] }'"

        assert "valid" in report_dict, m
        assert report_dict["valid"] == report.valid, m

        assert "errors" in report_dict, m
        assert len(report_dict["errors"]) == len(report.errors)

        assert "warnings" in report_dict, m
        assert len(report_dict["warnings"]) == len(report.warnings), m

        assert "stats" in report_dict
        assert isinstance(report_dict["stats"], dict), m
        assert "seconds" in report_dict["stats"], m

        assert "date" in formatted_report
        try:
            datetime.fromisoformat(formatted_report["date"])
        except ValueError:
            pytest.fail(f"{m}: fail to parse date properly")

        assert "version" in formatted_report

        def is_json_serializable(report_dict) -> bool:
            try:
                json.dumps(report_dict)
                return True
            except Exception:
                return False

        assert is_json_serializable(report_dict)
