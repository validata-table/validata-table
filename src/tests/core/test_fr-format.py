from copy import copy
from dataclasses import dataclass
from typing import Any, Dict, List, Type

from validata_core import resource_service, validation_service
from validata_core.domain.check import Check, new_check_repository
from validata_core.domain.types import CellValue, Error, ErrType
from validata_core.domain.types.field import Field
from validata_core.domain.validator import Validator, single_value


@dataclass
class ValidatorFactory:
    @staticmethod
    def single_valid_value(valid_value: str):
        class SingleValueValidator(Validator):
            def __init__(self, options):
                pass

            @single_value
            def validate(self, value):
                if value != valid_value:
                    return Error.new(
                        "Valeur invalide",
                        f"La valeur est différente de l'unique valeur valide {valid_value}",
                        ErrType.CUSTOM_CHECK_ERROR,
                    )

        return SingleValueValidator

    @staticmethod
    def forced() -> Type[Validator]:
        class ForcedValidator(Validator):
            """The Forced Validator will raise an error whenever an option
            "force-error" is set to "true"
            """

            def __init__(self, options):
                self._should_error = options.declare("force-error", bool, default=False)

            @single_value
            def validate(self, value):
                if self._should_error:
                    return Error.new(
                        "Échec Forcé",
                        "Le validateur a été forcé à retourner une erreur",
                        ErrType.CUSTOM_CHECK_ERROR,
                    )

        return ForcedValidator


def make_test_validation_service(test_repository):
    # Inject checks specific to tests
    test_validation_service = copy(validation_service)
    test_validation_service._custom_checks_repository = test_repository
    return test_validation_service


def _make_schema_with_field_custom_check(format, extra_field=False):
    fields = [{"name": "field1", Field._CUSTOM_CHECK_PROP: format}]

    if extra_field:
        fields.append({"name": "field2"})

    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": fields,
    }


def test_validation_with_field_custom_check():
    test_validation_service = make_test_validation_service(
        new_check_repository(
            [
                Check("siret", ValidatorFactory.single_valid_value("valid_siret")),
                Check("siren", ValidatorFactory.single_valid_value("valid_siren")),
            ]
        )
    )

    test_cases = [
        {
            "name": "Valid value, string field custom check",
            "custom_check": "siret",
            "value": "valid_siret",
            "expect_valid": True,
        },
        {
            "name": "Valid value 2, string field custom check",
            "custom_check": "siren",
            "value": "valid_siren",
            "expect_valid": True,
        },
        {
            "name": "Invalid value, string field custom check",
            "custom_check": "siret",
            "value": "invalid_siret",
            "expect_valid": False,
        },
        {
            "name": "Invalid value 2, string field custom check",
            "custom_check": "siren",
            "value": "invalid_siren",
            "expect_valid": False,
        },
        {
            "name": "Valid value for second field, string field custom check",
            "custom_check": "siret",
            "extra_field": True,
            "value": "valid_siret",
            "expect_valid": True,
        },
        {
            "name": "Valid value, object field custom check",
            "custom_check": {"name": "siret"},
            "value": "valid_siret",
            "expect_valid": True,
        },
        {
            "name": "Invalid value, object field custom check",
            "custom_check": {"name": "siren"},
            "value": "invalid_siren",
            "expect_valid": False,
        },
    ]

    for tc in test_cases:
        has_extra_field = "extra_field" in tc and tc["extra_field"]

        data = [{"field1": tc["value"]}]
        if has_extra_field:
            data[0]["field2"] = "some_value"

        schema = _make_schema_with_field_custom_check(
            tc["custom_check"], has_extra_field
        )

        validata_source = resource_service.from_inline_data(data)
        report = test_validation_service.validate_resource(validata_source, schema)

        assert (
            report.valid == tc["expect_valid"]
        ), f'tc["test_name"]\nReport first errors: {report.errors[:3]}'


def test_format_with_parameters():
    test_validation_service = make_test_validation_service(
        new_check_repository([Check("forced", ValidatorFactory.forced())])
    )
    test_cases = [
        {
            "test_name": (
                "Extra boolean parameter passed to object field custom check, "
                "validation is forced to True"
            ),
            "force_error": True,
            "expect_valid": False,
        },
        {
            "test_name": (
                "Extra boolean parameter passed to object field custom check, "
                "validation is forced to False"
            ),
            "force_error": False,
            "expect_valid": True,
        },
    ]

    for tc in test_cases:
        schema = _make_schema_with_field_custom_check(
            {"name": "forced", "force-error": tc["force_error"]}
        )

        data: Any = [{"field1": "whatever"}]
        validata_source = resource_service.from_inline_data(data)

        report = test_validation_service.validate_resource(validata_source, schema)
        assert (
            report.valid == tc["expect_valid"]
        ), f'tc["test_name"]\nReport first errors: {report.errors[:3]}'


def test_invalid_field_custom_check():
    test_cases = [
        {
            "custom_check": "invalid",
            "valid_formats": ["siren", "siret"],
        },
        {
            "custom_check": "siret",
            "valid_formats": [],
        },
        {
            "custom_check": "siret",
            "valid_formats": ["siren"],
        },
    ]

    ### Test helpers to test both str and object formats for field custom
    ### checks
    def _convert_field_custom_check_to_object_form(test_case):
        """Assumes field custom check is in string form"""
        test_case["custom_check"] = {"name": test_case["custom_check"]}
        return test_case

    for tc in test_cases:
        if isinstance(tc["custom_check"], str):
            test_cases.append(_convert_field_custom_check_to_object_form(tc))

    for tc in test_cases:
        schema = _make_schema_with_field_custom_check("FIELD", tc["custom_check"])
        validation_service = make_test_validation_service(
            new_check_repository(
                [
                    Check(f, ValidatorFactory.single_valid_value("valid"))
                    for f in tc["valid_formats"]
                ]
            )
        )
        inline_data: List[Dict[str, CellValue]] = [{"SIRET": "abc"}]
        validata_source = resource_service.from_inline_data(inline_data)
        report = validation_service.validate_resource(validata_source, schema)

        assert not report.valid
        errs = report.errors

        assert len(errs) == 1
        assert errs[0].type == ErrType.CHECK_ERROR


def test_single_invalid_format_does_not_prevent_subsequent_checks():
    test_validation_service = make_test_validation_service(
        new_check_repository(
            [Check("siret", ValidatorFactory.single_valid_value("valid_siret"))]
        )
    )

    tc = {
        "field1_custom_check": "invalid",
        "field2_custom_check": "siret",
        "field2_value": "invalid_siret",
        "expect_valid": False,
    }

    schema = _make_schema_with_field_custom_check(tc["field1_custom_check"], True)
    schema["fields"][1][Field._CUSTOM_CHECK_PROP] = tc["field2_custom_check"]

    data = [{"field1": "some value", "field2": tc["field2_value"]}]

    validata_source = resource_service.from_inline_data(data)
    report = test_validation_service.validate_resource(validata_source, schema)

    assert not report.valid
    errs = report.errors

    assert len(errs) == 2
    types = [err.type for err in errs]
    assert ErrType.CHECK_ERROR in types


def test_explicit_error_message():
    test_validation_service = make_test_validation_service(
        new_check_repository(
            [
                Check(
                    "blanquette",
                    ValidatorFactory.single_valid_value(
                        "Comment est votre blanquette ?"
                    ),
                ),
                Check("vie-univers", ValidatorFactory.single_valid_value("42")),
            ]
        )
    )
    test_cases = [
        {
            "name": "Invalid check 1 type",
            "custom_check": "blanquette",
            "expect_exact_string": "custom-check-error",
            "in_property": "type",
        },
        {
            "name": "Invalid check 2 type",
            "custom_check": "vie-univers",
            "expect_exact_string": "custom-check-error",
            "in_property": "type",
        },
    ]

    for tc in test_cases:
        data = [{"field1": "invalid"}]
        schema = _make_schema_with_field_custom_check(tc["custom_check"])
        validata_source = resource_service.from_inline_data(data)  # type: ignore
        report = test_validation_service.validate_resource(validata_source, schema)

        assert not report.valid
        errs = report.errors

        assert len(errs) == 1

        property_value = ""
        if tc["in_property"] == "type":
            property_value = errs[0].type.value

        if "expect_substring" in tc:
            assert tc["expect_substring"] in property_value

        if "expect_exact_string" in tc:
            assert tc["expect_exact_string"] == property_value


if __name__ == "__main__":
    test_validation_with_field_custom_check()
