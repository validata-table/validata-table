import frictionless
import frictionless.fields as frless_fields
import pytest

from validata_core.domain.types import Field, TypedException


def test_field_types():
    test_cases = [
        frless_fields.StringField,
        frless_fields.NumberField,
        frless_fields.IntegerField,
        frless_fields.BooleanField,
        frless_fields.ObjectField,
        frless_fields.ArrayField,
        frless_fields.DateField,
        frless_fields.TimeField,
        frless_fields.DatetimeField,
        frless_fields.YearField,
        frless_fields.YearmonthField,
        frless_fields.DurationField,
        frless_fields.GeopointField,
        frless_fields.GeojsonField,
        frless_fields.AnyField,
    ]
    for test_type in test_cases:
        try:
            Field.from_frictionless(test_type(name="name"))
        except Exception:
            pytest.fail()


def test_wrong_field_raises_typed_exception():
    """This situation _should_ never arise if our field types are aligned with
    the table schema types. Indeed, field type errors will be intercepted
    before trying to cast them into an enum.

    But in case a new type is introduced, it is better to have a proper error
    rather than an internal error.
    """

    class UnknownField(frictionless.Field):
        type = "unknown"

    frless_field = UnknownField(name="Unknown type")

    with pytest.raises(TypedException):
        Field.from_frictionless(frless_field)
