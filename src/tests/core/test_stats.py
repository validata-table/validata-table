from validata_core import validate


def test_row_stats():
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "abc",
                "type": "string",
            }
        ],
    }

    source = [["abc"], ["val1"]]

    report = validate(source, schema)

    assert report.stats.rows == 1

    source = [["abc"]]
    report = validate(source, schema)

    assert report.stats.rows == 0, "empty data"

    source = [["abc"]] + [[1]] * 1001
    report = validate(source, schema)

    assert report.stats.rows == 1001, "data length exceeding maximum errors"


def test_field_stats():
    schema = {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {"name": "abc", "type": "string", "constraints": {"required": True}}
        ],
    }

    source = [["notInSchema"]]

    report = validate(source, schema)
    assert report.stats.fields == 1
