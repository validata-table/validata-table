import pytest

from validata_core import validate
from validata_core.domain.types import ErrType


@pytest.fixture()
def schema_with_orphan_custom_check():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "siren",
                "title": "Numéro SIREN",
                "type": "string",
                "constraints": {"required": True},
            }
        ],
        "custom_checks": [
            {"name": "french-siren-value", "params": {"column": "siren"}}
        ],
    }


@pytest.fixture()
def schema_with_unknown_custom_check():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "siren",
                "title": "Numéro SIREN",
                "type": "string",
            }
        ],
        "custom_checks": [{"name": "foo-siren-value", "params": {"column": "sirene"}}],
    }


@pytest.fixture()
def schema_with_non_existing_primary_key():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "a",
            }
        ],
        "primaryKey": ["b"],
    }


@pytest.fixture()
def schema_with_existing_primary_key():
    return {
        "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
        "fields": [
            {
                "name": "a",
            }
        ],
        "primaryKey": ["a"],
    }


def test_schema_with_orphan_custom_checks(schema_with_orphan_custom_check):
    source = [["sirene"], ["529173189"]]
    report = validate(source, schema_with_orphan_custom_check)
    assert not report.valid

    assert len(report.errors) == 1
    assert report.errors[0].type == ErrType.MISSING_LABEL


def test_schema_with_unknown_custom_checks(schema_with_unknown_custom_check):
    source = [["siren"], ["529173189"]]
    report = validate(source, schema_with_unknown_custom_check)
    assert not report.valid

    assert len(report.errors) == 1
    assert report.stats.errors == 1
    assert report.errors[0].type == ErrType.CHECK_ERROR

    # What about 2 errors in the custom checks ?
    schema_with_unknown_custom_check["custom_checks"].append(
        {"name": "unknown2", "params": {"column": "siren"}}
    )
    report = validate(source, schema_with_unknown_custom_check)
    assert report.stats.errors == 2


def test_primary_key_schema(schema_with_non_existing_primary_key):
    source = [["a"], ["foo"]]

    report = validate(source, schema_with_non_existing_primary_key)
    assert not report.valid

    assert len(report.errors) == 1
    # as a 'schema-error'
    assert report.errors[0].type == ErrType.SCHEMA_ERROR


def test_primary_key_schema_2(schema_with_existing_primary_key):
    source = [["b"], ["foo"]]
    report = validate(source, schema_with_existing_primary_key)
    assert not report.valid

    assert len(report.errors) == 1
    assert report.errors[0].type == ErrType.MISSING_LABEL
