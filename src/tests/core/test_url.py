import pytest

from validata_core import resource_service
from validata_core.domain.types import ErrType, TypedException


def test_url_resource_without_meaningful_ext():
    url_404error = "https://demo.data.gouv.fr/fr/datasets/r/error404"
    with pytest.raises(TypedException) as err:
        resource_service.from_remote_file(url_404error)
        assert err.type == ErrType.SOURCE_ERROR
