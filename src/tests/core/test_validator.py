from typing import List, Sequence

from validata_core.domain.check import Check, FieldParam
from validata_core.domain.types import Error, ErrType, Tag
from validata_core.domain.validator import BaseValidator, FieldValue, single_value

from .utils import SchemaBuilder, new_validation_service


def test_single_value_check():
    class FailSingleValidator(BaseValidator):
        """Test validator that always fails

        The associated check will report an error, provided the field has a value.
        """

        @single_value
        def validate(self, value):
            return Error.new("Test", "Expected Failure For Testing", ErrType.CELL_ERROR)

    # Wrap the validator into a proper check
    fail_check_single = Check("fail", FailSingleValidator)

    # Create validation service including this test, with check id "fail"
    validation_service = new_validation_service(fail_check_single)

    # Create a schema that uses the custom check
    schema = (
        SchemaBuilder()
        .add_field("id", "number")
        .add_field("col1", "number", customCheck="fail")
        .get()
    )

    test_cases = {
        "custom check errors if value": {
            "source": [["col1"], [1]],
            "expect_error": True,
        },
        "no validation if value is missing": {
            # "id" value is added to avoid "empty line" errors
            "source": [["id", "col1"], [1, None]],
            "expect_error": False,
        },
        "0 (falsy) integer is validated": {
            "source": [["col1"], [0]],
            "expect_error": True,
        },
    }

    for name, tc in test_cases.items():
        report = validation_service.validate(tc["source"], schema_descriptor=schema)

        if ("expect_error" not in tc) or not tc["expect_error"]:
            assert report.valid, name
        else:
            assert not report.valid, name
            assert len(report.errors) == 1
            error_dict = report.errors[0].to_dict()

            tags: Sequence[str] = error_dict["tags"]  # type: ignore
            assert Tag.TABLE.value in tags, report.to_dict()
            assert Tag.ROW.value in tags
            assert Tag.CELL.value in tags

            assert error_dict["fieldName"] == "col1"


def test_multi_value_check():
    class FailMultiValidator(BaseValidator):
        """Test validator with multiple fields, that always fails.

        The associated check will report errors, provided all fields have a value.
        """

        def validate(self, field: FieldValue, other_fields: List[FieldValue]):
            return Error.new("Test", "Expected Failure For Testing", ErrType.CELL_ERROR)

    fail_check_multi = Check(
        "fail-multi",
        FailMultiValidator,
        field_params=[
            "column",
            FieldParam("other_columns", holds_multiple=True),
        ],
    )

    validation_service = new_validation_service(fail_check_multi)

    schema = (
        SchemaBuilder()
        .add_field(
            "col1",
            "number",
            customCheck={"name": "fail-multi", "other_columns": ["col2", "col3"]},
        )
        .add_field("col2")
        .add_field("col3", "number")
        .get()
    )

    test_cases = {
        "custom check errors if all values": {
            "source": [["col1", "col2", "col3"], [1, "abc", 0]],
            "expect_error": True,
        },
        "validation does not occur if value is missing": {
            "source": [["col1", "col2", "col3"], [1, "abc", None]],
            "expect_error": False,
        },
        "validation does not occur if value is empty string": {
            "source": [["col1", "col2", "col3"], [1, "", 1]],
            "expect_error": False,
        },
    }

    for name, tc in test_cases.items():
        report = validation_service.validate(tc["source"], schema_descriptor=schema)

        if ("expect_error" not in tc) or not tc["expect_error"]:
            print(name)
            assert report.valid, name
        else:
            assert not report.valid, name
            assert len(report.errors) == 1
            error_dict = report.errors[0].to_dict()

            tags: Sequence[str] = error_dict["tags"]  # type: ignore
            assert Tag.TABLE.value in tags
            assert Tag.ROW.value in tags
            assert Tag.CELL.value in tags

            assert error_dict["fieldName"] == "col1"
