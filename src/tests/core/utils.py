from copy import deepcopy
from typing import Dict, Optional

from validata_core import validation_service
from validata_core.domain.check import Check, new_check_repository
from validata_core.domain.types import Report


class SchemaBuilder:
    """A simple test utility class to help at schema creation"""

    def __init__(self):
        self._schema = {
            "$schema": "https://frictionlessdata.io/schemas/table-schema.json",
            "fields": [],
        }

    def add_field(self, name: str, type_: str = "string", **kwargs):
        self._schema["fields"].append({"name": name, "type": type_, **kwargs})
        return self

    def get(self) -> Dict:
        return self._schema


def new_validation_service(custom_check: Optional[Check] = None):
    fake_service = deepcopy(validation_service)
    if custom_check:
        fake_service._custom_checks_repository = new_check_repository([custom_check])
    return fake_service


def assert_valid_report(report: Report) -> None:
    assert report.valid, report.to_dict()
    assert not report.errors
    assert report.stats.errors == 0


def assert_single_error(report: Report) -> None:
    assert not report.valid
    assert report.stats.errors == 1
    assert len(report.errors) == 1


def assert_no_warning(report: Report) -> None:
    assert report.stats.warnings == 0
    assert not report.warnings


def assert_single_warning(report: Report) -> None:
    assert report.stats.warnings == 1
    assert len(report.warnings) == 1
