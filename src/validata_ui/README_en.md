# Validata Table UI

Validata Table user interface

## Usage

You can use the online instance of Validata Table User Interface https://validata.fr/

## Run in local development environment

Start the docker development environment ...

```bash
make serve_dev
```

... then open http://localhost:5000/
