��    %      D              l  n   m     �     �  �        �     �  /   �     �             )        C  9   J  T   �  2   �               (     8     L     b     w     ~     �  )   �  .   �     �          0     F  9   O     �     �  &   �     �     �  �  �  o   �     �     	  ~   	     �	     �	  *   �	     �	     
     
  7   %
     ]
  +   d
  d   �
  /   �
     %     +     8     E     b     u     �     �     �  (   �  /   �  #        /     ?  	   Q  C   [     �     �  -   �     �     �   %(validata_clickable)s est une initiative d'%(opendatafrance_clickable)s développée par %(multi_clickable)s. 1 erreur détectée 1 recommandation Afin d'assurer la conformité optimale de vos données par rapport au schéma, merci de prendre en compte les recommandations ci-dessous Aucune erreur détectée Catalogue indisponible Cette colonne n'est pas définie dans le schema Choisissez un schéma Colonne Colonne inconnue Combinaison de paramètres non supportée Erreur Erreur dans la récupération des informations de schéma Indiquez ici l'URL d'un schéma que vous souhaitez utiliser pour valider un fichier. Le schéma fourni n'est pas un fichier JSON valide Ligne Rapport de validation Recommandations Schéma à la carte Signaler un problème Télécharger en PDF Valeur Valider le fichier Valider un fichier Vous n'avez pas indiqué d'URL à valider Vous n'avez pas indiqué de fichier à valider contactez votre administrateur erreurs détectées format JSON incorrect invalide l'encodage du fichier est invalide. Veuillez le corriger. problème de connexion recommandations service de génération non configuré valide version Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2024-07-11 22:36+0200
PO-Revision-Date: 2024-02-27 10:30+0000
Last-Translator: Dr. Jesper Zedlitz <jesper.zedlitz@stk.landsh.de>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.15.0
 %(validata_clickable)s ist eine Initiative von %(opendatafrance_clickable)s entwickelt von %(multi_clickable)s. 1 Fehler gefunden 1 Empfehlung Um eine optimale Übereinstimmung Ihrer Daten mit dem Schema zu gewährleisten, beachten Sie bitte die folgenden Empfehlungen. Kein Fehler gefunden Katalog nicht verfügbar Diese Spalte ist nicht im Schema definiert Wählen Sie ein Schema Spalte Unbekannte Spalte Diese Kombination von Parameter wird nicht unterstützt Fehler Fehler beim Abrufen von Schemainformationen Geben Sie hier die Adresse eines Schemas an, das Sie zur Validierung einer Datei verwenden möchten. Sie haben keine zu validierende Datei angegeben Zeile Prüfbericht Empfehlungen Ein eigenes Schema verwenden Ein Problem melden Als PDF herunterladen Wert Datei validieren Datei validieren Sie haben keinen gültigen URL angegeben Sie haben keine zu validierende Datei angegeben Kontaktieren Sie den Systembetreuer Fehler gefunden fehlerhaftes JSOn ungültig Der Zeichensatz der Datei ist ungültig. Bitte korrigieren Sie dies Verbindungsfehler Empfehlungen Der Generierungsdienst ist nicht eingerichtet gültig Version 